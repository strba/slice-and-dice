## SASS

Sass is used on this simple web page.

**Installation (Linux) and Run:**

1. Install Ruby
```bash
sudo apt-get install ruby-full
```
2. Install Sass
```bash
sudo gem install sass
```
3. Run Sass on this project
```bash
sass --watch sass:public/stylesheets
```
If you want new sass files, be sure to upload them to the * -dir.sass file located in the directory where you created the file.

---