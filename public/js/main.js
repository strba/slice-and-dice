const btn = document.getElementById("hamburger_button");
const navCollapse = document.getElementById("nav_collapse");
const nav =  document.getElementById("nav_wrapper");
const primeArticle = document.getElementById("prime_article");

btn.onclick = function onclick(event) {
    if (navCollapse.classList.contains("nav_collapse__active") === false) {
        navCollapse.classList.add('nav_collapse__active')
        this.classList.add('_nav__hamburger_button__active')
    } else {
        {
            navCollapse.classList.remove('nav_collapse__active')
            this.classList.remove('_nav__hamburger_button__active')
        }
    }
}


window.addEventListener("scroll", function() {
    window.scrollY > 1?
    nav.classList.add('nav__fixed'): nav.classList.remove('nav__fixed')
});
